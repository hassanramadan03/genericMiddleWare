const cors = require('cors'),
    express = require('express'),
    bodyParser = require('body-parser'),
    config=require('./config/config'),
    mongoose=require('mongoose'),
    helmet = require('helmet'),
    app = express();
    app.locals.moment=require('moment'),
    mongoose.connect(config.online , config.mongoOptions);    
//routing files
const routes=require('./business-modules/routes')
 

app.set('port', (process.env.PORT || 8080));
app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(cors({
    'allowedHeaders': ['sessionId', 'Content-Type'],
    'exposedHeaders': ['sessionId'],
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
  }));
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
}));
 

 
app.use(bodyParser.urlencoded({
    extended: true
}));
 
// Force HTTPS like Heroku
if (app.get('env') === 'production') {
    app.use(function(req, res, next) {
        var protocol = req.get('x-forwarded-proto');
        protocol == 'https' ? next() : res.redirect('https://' + req.hostname + req.url);
    });
}

// update channels
 
app.use('/meet',routes.meetRoute);
 
 
 

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    app.listen(app.get('port'), function() {
        console.log('Node app is running on port', app.get('port'));
    });
});

exports = module.exports = app;

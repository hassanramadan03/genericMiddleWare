var mongoose = require('mongoose');
var meetingSchema = new mongoose.Schema({
   userId: mongoose.Schema.Types.ObjectId,
   meetingKey:{type:String,default:''},
   invitedUsers:[mongoose.Schema.Types.ObjectId],
   roomId: mongoose.Schema.Types.ObjectId,

   
});
module.exports = mongoose.model('meeting', meetingSchema);

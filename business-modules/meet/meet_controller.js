const renderResponseUtil = require('../../utils/RenderResponseUtil');
const Meet_service = require('./meet_service');
module.exports = {
    openMeet,
    getMeetKey, 
    deleteMeet,
    getAllMeets

}

async function openMeet(req, res) {
    try {
        console.log(req.body);

        const userId = req.body.userId;
        const invitedUsers = req.body.invitedUsers;
        const roomId = req.body.roomId;

        const requestResult = await Meet_service.openMeet(userId, invitedUsers,  roomId);
        renderResponseUtil.sendResponse(req, res, requestResult);

    } catch (error) {
        res.send(error);
    }

}

async function getMeetKey(req, res) {
    try {

        const invitedUsers = req.body.invitedUsers;
        const roomId = req.body.roomId;

        const requestResult = await Meet_service.getMeetKey(invitedUsers, roomId);
        renderResponseUtil.sendResponse(req, res, requestResult);

    } catch (error) {
        res.send(error);
    }

}
async function deleteMeet(req, res) {
    try {
        const meetingKey = req.body.meetingKey
        const requestResult = await Meet_service.deleteMeeting(meetingKey);
        renderResponseUtil.sendResponse(req, res, requestResult);

    } catch (error) {
        res.send(error);
    }

}
async function getAllMeets(req, res) {
    try {
        const requestResult = await Meet_service.getAllMeets();
        renderResponseUtil.sendResponse(req, res, requestResult);

    } catch (error) {
        res.send(error);
    }

}
const express = require('express');
const router = express.Router();
const meet_controller = require('./meet_controller');

 
router.post('/open',meet_controller.openMeet);
router.post('/getKey',meet_controller.getMeetKey);
router.post('/delete',meet_controller.deleteMeet);
router.get('/getAll',meet_controller.getAllMeets);
 

 

module.exports = router;
const openMeeting = require('../../models/meeting');
module.exports = {
    openMeet,
    getMeetKey,
    deleteMeeting,
    getAllMeets

}


function openMeet(userId, invitedUsers, meetingKey, roomId) {
    return new Promise(async (resolve, reject) => {
        try {
            const newMeeting = new openMeeting({
                userId: userId,
                invitedUsers: invitedUsers,
                meetingKey: meetingKey,
                roomId: roomId
            })
            const openedMeeting = await newMeeting.save();
            //update the meeting key async
            newMeeting.update({ meetingKey: openedMeeting._id }, function (error, result) {
                if (error) resolve(error)
                else {
                    console.log(result);
                    
                    return;
                }
            })
            resolve({ meetingKey: openedMeeting._id });
        }
        catch (error) {
            reject(error)
        }
    })
}
function getMeetKey(invitedUsers, roomId) {
    return new Promise(async (resolve, reject) => {
        try {
            const meetKey = openMeeting.findOne({ roomId: roomId, invitedUsers: invitedUsers }, { meetingKey: 1 })
            resolve(meetKey);
        }
        catch (error) {
            reject(error)
        }
    })
}
function deleteMeeting(meetingKey) {
    return new Promise(async (resolve, reject) => {
        try {
            console.log(meetingKey);

            const deletedMeet = openMeeting.remove({ meetingKey: meetingKey })
            resolve(deletedMeet);
        }
        catch (error) {
            reject(error)
        }
    })
}
function getAllMeets() {
    return new Promise(async (resolve, reject) => {
        try {
            const openedMeeting = await openMeeting.find({});
            resolve(openedMeeting);
        }
        catch (error) {
            reject(error)
        }
    })
}